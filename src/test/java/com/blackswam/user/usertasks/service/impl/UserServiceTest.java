package com.blackswam.user.usertasks.service.impl;

import com.blackswam.user.usertasks.entity.UserEntity;
import com.blackswam.user.usertasks.exceptions.UserNotFoundException;
import com.blackswam.user.usertasks.model.User;
import com.blackswam.user.usertasks.repository.UserRepository;
import com.blackswam.user.usertasks.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private UserEntity userEntity;


    @Before
    public void setUp() throws Exception {
        userEntity =  new UserEntity("test","abc","pqr");
        userEntity = userRepository.save(userEntity);
     }



    @Test
    public void createUser() {
       User user = new User();
       user.setUserName("test1");
       user.setFirstName("abc1");
       user.setLastName("pqr1");

        user = userService.createUser(user);
        assertEquals("test1",user.getUserName());
    }

    @Test
    public void getAllUsers() {

        List<User> users = userService.getAllUsers();
        assertNotNull(users);

    }

   // @Test
    public void findUserById() throws UserNotFoundException {
        User user = userService.findUserById(1);
        assertEquals(userEntity.getId(),user.getId());

    }

    @Test
    public void updateUser() throws UserNotFoundException{
        User user = userService.findUserById(1);
        user.setLastName("pqr12");
        User userObj = userService.updateUser(user);
        assertEquals(user.getLastName(),userObj.getLastName());

    }



}