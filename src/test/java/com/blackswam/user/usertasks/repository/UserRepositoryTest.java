package com.blackswam.user.usertasks.repository;

import com.blackswam.user.usertasks.entity.UserEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup(){
        userRepository.save(getMockUserEntity());
    }

    @Test
    public void listUsers(){
        List<UserEntity> userEntities = userRepository.findAll();
        assertNotNull(userEntities);
    }


    private UserEntity getMockUserEntity(){
       return new UserEntity("pk","phani","kalakuri");

    }

}