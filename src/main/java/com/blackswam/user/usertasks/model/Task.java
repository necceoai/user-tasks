package com.blackswam.user.usertasks.model;

import java.time.LocalDateTime;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

/**
 * Task
 */
@Validated

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-28T16:11:28.206Z")
@Getter
@Setter
@NoArgsConstructor
public class Task   {
  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("userId")
  private Integer userId = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("status")
  private String status = null;


  @JsonProperty("date_time")
  private String dateTime;

  @JsonProperty("createdDate")
  private LocalDateTime createdDate = null;

  @JsonProperty("updatedDate")
  private LocalDateTime updatedDate = null;



}

