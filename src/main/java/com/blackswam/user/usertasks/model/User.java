package com.blackswam.user.usertasks.model;

import java.time.LocalDateTime;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

/**
 * User
 */
@Validated


@Getter
@Setter
@NoArgsConstructor
public class User   {
  @JsonProperty("userName")
  private String userName;

  @JsonProperty("firstName")
  private String firstName ;


  @JsonProperty("lastName")
  private String lastName ;

  @JsonProperty("createdDate")
  private LocalDateTime createdDate ;

  @JsonProperty("updatedDate")
  private LocalDateTime updatedDate;

  @JsonProperty("id")
  private int id ;

  public User(String username, String firstName, String lastName) {
    this.userName = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.id = id;
  }


}

