package com.blackswam.user.usertasks.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Date;

@Entity(name = "task")
@Getter @Setter @NoArgsConstructor
public class TaskEntity {

    @Id
    @GeneratedValue
    private Integer id;


    private Integer userId ;


    private String name;


    private String description ;


    private String status ;

    @CreatedDate
    private LocalDateTime createdDate ;

    @LastModifiedDate
    private LocalDateTime updatedDate ;

    private LocalDateTime dateTime;
}
