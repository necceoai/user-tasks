package com.blackswam.user.usertasks;

import com.blackswam.user.usertasks.service.UserTaskJobService;
import org.quartz.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication

public class UserTasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserTasksApplication.class, args);
	}



}
