package com.blackswam.user.usertasks.service;


import com.blackswam.user.usertasks.entity.UserEntity;
import com.blackswam.user.usertasks.exceptions.UserNotFoundException;
import com.blackswam.user.usertasks.factory.Factory;
import com.blackswam.user.usertasks.model.User;
import com.blackswam.user.usertasks.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


     /**
     * Create User service
     * @param user
     * @return
     */
    public User createUser(User user){

        UserEntity userEntity = Factory.createWith(user);
        userEntity.setCreatedDate(LocalDateTime.now());
        userEntity = userRepository.save(userEntity);
        return Factory.createWith(userEntity);
    }

    /**
     *
     * @return
     */
    public List<User> getAllUsers() {
        List<UserEntity> userEntities = userRepository.findAll();
        return   userEntities.stream().map(userEntity -> {
            return Factory.createWith(userEntity);
        }).collect(Collectors.toList());
    }


    /**
     *
     * @param userId
     * @return
     * @throws UserNotFoundException
     */
    public User findUserById(int userId) throws UserNotFoundException {
        Optional<UserEntity> userEntity = userRepository.findById(userId);
        userEntity.orElseThrow(() -> new UserNotFoundException("1001","User Not Found"));
        return Factory.createWith(userEntity.get());
    }


    /***
     *
     * @param user
     * @return
     * @throws UserNotFoundException
     */
    public User updateUser(User user) throws UserNotFoundException{
        Optional<UserEntity> userEntity = userRepository.findById(user.getId());
        userEntity.orElseThrow(() -> new UserNotFoundException("1001","User Not Found"));
        UserEntity userEntityObj = Factory.createWith(user);
        userEntityObj.setUpdatedDate(LocalDateTime.now());
        userEntityObj.setCreatedDate(userEntity.get().getCreatedDate());
        userEntityObj = userRepository.saveAndFlush(userEntityObj);
        return Factory.createWith(userEntityObj);

    }

}
