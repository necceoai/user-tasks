package com.blackswam.user.usertasks.service;

import com.blackswam.user.usertasks.exceptions.ApiException;
import com.blackswam.user.usertasks.exceptions.TaskNotFoundException;
import com.blackswam.user.usertasks.model.Task;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


/**
 *
 */

@Service
public class UserTaskJobService extends QuartzJobBean {

    @Autowired
    private TaskService taskService;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {


            List<Task> taskList = taskService.findTaskByDateTime(LocalDateTime.now());
            System.out.println("=========== Pending Tasks ================");
            taskList.forEach(task -> {
                System.out.println(" Name " + task.getName());
                System.out.println("Description " + task.getDescription());
                System.out.println(" Time " + task.getDateTime());
                System.out.println("Updating the status to done!!!!");
                try {
                    taskService.updateStatus(task.getId());
                } catch (TaskNotFoundException e) {
                    e.printStackTrace();
                }

            });
            System.out.println("=========== End Job  ================");


    }
}
