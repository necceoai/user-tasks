package com.blackswam.user.usertasks.service;

import com.blackswam.user.usertasks.entity.TaskEntity;
import com.blackswam.user.usertasks.exceptions.ApiException;
import com.blackswam.user.usertasks.exceptions.BadRequestException;
import com.blackswam.user.usertasks.exceptions.TaskNotFoundException;
import com.blackswam.user.usertasks.factory.Factory;
import com.blackswam.user.usertasks.model.Task;
import com.blackswam.user.usertasks.repository.TaskRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.blackswam.user.usertasks.util.Helper.*;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;


   // private final ModelMapper modelMapper = new ModelMapper();

    private final String PENDING ="Pending";

    private final String DONE = "Done";

   /* PropertyMap<TaskEntity,Task> skipDateTimePropertyMap = new PropertyMap<TaskEntity, Task>() {
        @Override
        protected void configure() {
            skip().setDateTime(null);
        }
    };*/

    /**
     *
     * @param task
     * @return
     */
    public Task createTask(Task task) throws ApiException {

            TaskEntity taskEntity = Factory.createWith(task);
            taskEntity.setDateTime(getLocalDateTime(task.getDateTime()).getLocalDateTime());
            taskEntity.setStatus(PENDING);
            taskEntity = taskRepository.save(taskEntity);
            taskEntity.setCreatedDate(LocalDateTime.now());
            return Factory.createWith(taskEntity);
    }

    /**
     *
     * @param userId
     * @param taskId
     * @return
     * @throws TaskNotFoundException
     */
    public Task getTaskByIdAndUserId(int taskId,int userId) throws TaskNotFoundException{
       Optional<TaskEntity> taskEntity =  Optional.of(taskRepository.findByIdAndUserId(taskId,userId));
       taskEntity.orElseThrow(()  -> new TaskNotFoundException("1002","No Task Found"));
       return Factory.createWith(taskEntity.get());
    }


    /**
     *
     * @param taskId
     * @return
     * @throws TaskNotFoundException
     */
    public Task getTaskById(int taskId) throws TaskNotFoundException{
        Optional<TaskEntity> taskEntity = taskRepository.findById(taskId);
        taskEntity.orElseThrow(()  -> new TaskNotFoundException("1002","No Task Found"));
        return Factory.createWith(taskEntity.get());

    }

    /***
     *
     * @param userId
     * @return
     * @throws TaskNotFoundException
     */

    public List<Task> getAllTasksByUserId(int userId) throws TaskNotFoundException{
        List<TaskEntity> taskEntities = taskRepository.getAllByUserId(userId);
       return taskEntities.stream().map(taskEntity -> {
            return Factory.createWith(taskEntity);
        }).collect(Collectors.toList());
    }


    /***
     *
     * @return
     * @throws TaskNotFoundException
     */
    public List<Task> getAllTasks() throws TaskNotFoundException{
        List<TaskEntity> taskEntities = taskRepository.findAll();
        return taskEntities.stream().map(taskEntity -> {
            return Factory.createWith(taskEntity);
        }).collect(Collectors.toList());
    }

    /**
     *
     * @param task
     * @return
     * @throws TaskNotFoundException
     */

    public Task updateTask(Task task) throws  TaskNotFoundException,BadRequestException {
        Optional<TaskEntity> taskEntity = taskRepository.findById(task.getId());
        taskEntity.orElseThrow(() -> new TaskNotFoundException("1002","No Task Found"));
        TaskEntity taskEntityObj = Factory.createWith(task);
        taskEntityObj.setUpdatedDate(LocalDateTime.now());
        taskEntityObj = taskRepository.save(taskEntityObj);
        return Factory.createWith(taskEntityObj);
    }

    /**
     *
     * @param id
     * @throws TaskNotFoundException
     */
    public void updateStatus(int id) throws  TaskNotFoundException {
        Optional<TaskEntity> taskEntity = taskRepository.findById(id);
        taskEntity.orElseThrow(() -> new TaskNotFoundException("1002","No Task Found"));
        taskEntity.get().setStatus("Done");
        taskEntity.get().setUpdatedDate(LocalDateTime.now());
         taskRepository.save(taskEntity.get());
    }

    /**
     *
     * @param localDateTime
     * @return
     */

    public List<Task> findTaskByDateTime(LocalDateTime localDateTime){
        List<TaskEntity> taskEntities = taskRepository.findByDateTimeLessThanAndStatusEquals(localDateTime,PENDING);
        return taskEntities.stream().map(taskEntity -> {
            return Factory.createWith(taskEntity);
        }).collect(Collectors.toList());

    }


}
