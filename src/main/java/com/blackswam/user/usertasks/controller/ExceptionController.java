package com.blackswam.user.usertasks.controller;

import com.blackswam.user.usertasks.exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(ExceptionController.class);

	/**
	 * To Handle any internal server exceptions
	 *
	 * @param ex the uncaught exception
	 * @return an error response with status 500
	 */
	@ExceptionHandler(ApiException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<ErrorResponse> handleApiException(final HttpServletRequest request, final ApiException ex) {
		log.error("Internal Server Exception", ex);
		ErrorResponse errorResponse = buildErrorResponse(ex, ex.getCode(), ex.getMessage(), request);
		return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}


	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseEntity<ErrorResponse> handleUserNotFoundException(final HttpServletRequest request, final UserNotFoundException ex) {
		log.error("User not found thrown", ex);
		ErrorResponse errorResponse = buildErrorResponse(ex, ex.getCode(), ex.getMessage(), request);		
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BadRequestException.class)
	@ResponseBody
	public ResponseEntity<ErrorResponse> handlerBadRequestException(final HttpServletRequest request, final BadRequestException ex) {
		log.error("Bad Request Exception ", ex);
		ErrorResponse errorResponse = buildErrorResponse(ex, ex.getCode(), ex.getMessage(), request);		
		
		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
	}



	@ExceptionHandler(TaskNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ResponseEntity<ErrorResponse> TaskNotFoundException(final HttpServletRequest request, final TaskNotFoundException ex) {

		ErrorResponse errorResponse = buildErrorResponse(ex, ex.getCode(), ex.getMessage(), request);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}



    private ErrorResponse buildErrorResponse(final Exception ae, final String status, final String message, HttpServletRequest request){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.withStatus(status)
                .withError(ae.getLocalizedMessage())
                .withException(ae.getClass().getName())
                .withMessage(message)
                .withPath(request.getRequestURI());
        return errorResponse;
    }

}