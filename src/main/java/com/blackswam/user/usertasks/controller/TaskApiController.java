package com.blackswam.user.usertasks.controller;

import com.blackswam.user.usertasks.exceptions.ApiException;
import com.blackswam.user.usertasks.exceptions.TaskNotFoundException;
import com.blackswam.user.usertasks.model.Task;

import com.blackswam.user.usertasks.model.User;
import com.blackswam.user.usertasks.service.TaskService;
import com.blackswam.user.usertasks.util.Helper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import static com.blackswam.user.usertasks.util.Helper.*;

@RestController
public class TaskApiController  {

    private static final Logger log = LoggerFactory.getLogger(UserApiController.class);

    @Autowired
    private TaskService taskService;

    private final HttpServletRequest request;

    @Autowired
    public TaskApiController(ObjectMapper objectMapper, HttpServletRequest request) {

        this.request = request;
    }

    /**
     *
     * @param id
     * @return
     * @throws ApiException
     */

    @GetMapping(value = "/user/tasks/{id}",produces =  "application/json" ,
    consumes = "application/json" )
    public ResponseEntity<Task> getTask( @PathVariable("id") int id) throws ApiException {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            Task task = taskService.getTaskById(id);
            return new ResponseEntity<Task>(task,HttpStatus.OK);
        }

        return new ResponseEntity<Task>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }


    @GetMapping(value = "/user/{userId}/tasks/{id}",produces =  "application/json" ,
            consumes = "application/json" )
    public ResponseEntity<Task> getTaskByIdAndUserId(@PathVariable("userId") int userId, @PathVariable("id") int id) throws ApiException {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            Task task = taskService.getTaskByIdAndUserId(id,userId);
            return new ResponseEntity<Task>(task,HttpStatus.OK);
        }

        return new ResponseEntity<Task>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @GetMapping(value = "/user/{userId}/tasks",produces =  "application/json" ,
            consumes = "application/json" )
    public ResponseEntity<List<Task>> getTasksByUserId(@PathVariable("userId") int userId) throws ApiException {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            List<Task> tasks = taskService.getAllTasksByUserId(userId);
            if(tasks!=null && tasks.size() > 0){
                return new ResponseEntity<>(tasks,HttpStatus.OK);
            }
            return new ResponseEntity<>(Arrays.asList(),HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     *
     * @return
     * @throws ApiException
     */
    @GetMapping(value = "/user/tasks",produces =  "application/json" ,
            consumes = "application/json" )

    public ResponseEntity<List<Task>> listTasks() throws ApiException {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            List<Task> tasks = taskService.getAllTasks();
            if(tasks!=null && tasks.size() > 0){
                return new ResponseEntity<>(tasks,HttpStatus.OK);
            }
            return new ResponseEntity<>(Arrays.asList(),HttpStatus.OK);
        }

        return new ResponseEntity<List<Task>>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     *
     * @param body
     * @return
     * @throws ApiException
     */

    @PostMapping (value = "/user/{userId}/tasks",produces =  "application/json" ,
            consumes = "application/json" )
    public ResponseEntity<Task> createTask( @PathVariable("userId") int userId,  @RequestBody Task body) throws ApiException{
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
              if(getLocalDateTime(body.getDateTime()).isValid()){

                body.setUserId(userId);
                return new ResponseEntity<>(taskService.createTask(body), HttpStatus.OK);
            }
        }

        return new ResponseEntity<Task>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     *
     * @param id
     * @param body
     * @return
     * @throws ApiException
     */
    @PutMapping(value = "/user/tasks/{id}",produces =  "application/json" ,
            consumes = "application/json" )
    public ResponseEntity<Task> updateTask( @PathVariable("id") int id,  @Valid @RequestBody Task body) throws ApiException{
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if(getLocalDateTime(body.getDateTime()).isValid()) {
                body.setId(id);
                return new ResponseEntity<>(taskService.updateTask(body), HttpStatus.OK);
            }
        }

        return new ResponseEntity<Task>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     *
     * @param id
     * @return
     * @throws ApiException
     */
    @DeleteMapping (value = "/user/tasks/{id}",produces =  "application/json" ,
            consumes = "application/json" )
    public ResponseEntity deleteTask(@PathVariable("id") int id) throws ApiException {
        taskService.updateStatus(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
