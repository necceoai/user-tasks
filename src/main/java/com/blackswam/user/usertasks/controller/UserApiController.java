package com.blackswam.user.usertasks.controller;

import com.blackswam.user.usertasks.exceptions.ApiException;
import com.blackswam.user.usertasks.model.User;
import com.blackswam.user.usertasks.service.UserService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@RestController
public class UserApiController  {

    private static final Logger log = LoggerFactory.getLogger(UserApiController.class);
    private final HttpServletRequest request;

    @Autowired
    private UserService userService;

    @Autowired
    public UserApiController(HttpServletRequest request) {
        this.request = request;
    }


    /***
     *
     * @param user
     * @return
     * @throws ApiException
     */

    @PostMapping(value = "/user", produces = "application/json", consumes = "application/json")
    public ResponseEntity<User> createUser(  @Valid @RequestBody User user) throws ApiException {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
               User userObj =   userService.createUser(user);
               return new ResponseEntity<User>(userObj,HttpStatus.OK);

        }
        return new ResponseEntity<User>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }


    /**
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/user/{id}", produces = "application/json")
    public ResponseEntity<User> getUser( @PathVariable("id") int id) throws ApiException {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            User user = userService.findUserById(id);
            return new ResponseEntity<User>(user,HttpStatus.OK);
        }

        return new ResponseEntity<User>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    /**
     *
     * @return
     */
    @GetMapping(value = "/user", produces = "application/json")
    public ResponseEntity<List<User>> getUsers() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            List<User> userList = userService.getAllUsers();
            if (userList != null && userList.size() >0){
                return new ResponseEntity<List<User>>(userList,HttpStatus.OK);
            }
            return new ResponseEntity<List<User>>(Arrays.asList(),HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }


    /**
     *
     * @param id
     * @param body
     * @return
     * @throws ApiException
     */
    @PutMapping(value = "/user/{id}", produces = "application/json")
    public ResponseEntity<User> updateUser( @PathVariable("id") int id, @Valid @RequestBody User body) throws ApiException{
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            body.setId(id);
            User user = userService.updateUser(body);
            return new ResponseEntity<User>(user,HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

}
