package com.blackswam.user.usertasks.repository;


import com.blackswam.user.usertasks.entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity,Integer> {

    public TaskEntity findByIdAndUserId(int taskId,int userId);

    public List<TaskEntity> getAllByUserId(int userId);

    public List<TaskEntity> findByDateTimeLessThanAndStatusEquals(LocalDateTime localDateTime,String status);
}
