package com.blackswam.user.usertasks.repository;

import com.blackswam.user.usertasks.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Integer>  {
}
