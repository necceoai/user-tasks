package com.blackswam.user.usertasks.exceptions;

public class TaskNotFoundException extends ApiException {
    public TaskNotFoundException(String code, String msg) {
        super(code, msg);
    }
}
