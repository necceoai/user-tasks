package com.blackswam.user.usertasks.exceptions;

public class BadRequestException extends  ApiException {

    public BadRequestException(String code, String msg) {
        super(code, msg);
    }
}
