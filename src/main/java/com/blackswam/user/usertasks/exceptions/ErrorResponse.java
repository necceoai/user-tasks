package com.blackswam.user.usertasks.exceptions;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;



import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "timestamp",
        "status",
        "error",
        "exception",
        "messages",
        "path"
})
public class ErrorResponse implements Serializable
{

    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private String timestamp;
    @JsonProperty("status")
    private String status;
    @JsonProperty("error")
    private String error;
    @JsonProperty("exception")
    private String exception;
    @JsonProperty("messages")
    private String message = null;
    @JsonProperty("path")
    private String path;
    private final static long serialVersionUID = -520620378412486216L;

    public  ErrorResponse(){
        this.timestamp = LocalDateTime.now().toString();
    }

    @JsonProperty("timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp =  LocalDateTime.now().toString();
    }



    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public ErrorResponse withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    public ErrorResponse withError(String error) {
        this.error = error;
        return this;
    }

    @JsonProperty("exception")
    public String getException() {
        return exception;
    }

    @JsonProperty("exception")
    public void setException(String exception) {
        this.exception = exception;
    }

    public ErrorResponse withException(String exception) {
        this.exception = exception;
        return this;
    }

    @JsonProperty("messages")
    public String getMessage() {
        return message;
    }

    @JsonProperty("messages")
    public void setMessages(String messages) {
        this.message = messages;
    }

    public ErrorResponse withMessage(String messages) {
        this.message = messages;
        return this;
    }


    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    public ErrorResponse withPath(String path) {
        this.path = path;
        return this;
    }


}