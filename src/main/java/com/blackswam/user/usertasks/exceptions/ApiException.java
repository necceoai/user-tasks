package com.blackswam.user.usertasks.exceptions;


import org.springframework.http.HttpStatus;

public class ApiException extends Exception{
    private static final long serialVersionUID = -7971799398188092961L;

    private String code;
    private String errorMessage;
    private HttpStatus httpStatus;

    public ApiException(String code, String msg) {
        super(msg);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
