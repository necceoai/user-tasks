package com.blackswam.user.usertasks.exceptions;

public class UserNotFoundException extends ApiException {
    public UserNotFoundException(String code, String msg) {
        super(code, msg);
    }
}
