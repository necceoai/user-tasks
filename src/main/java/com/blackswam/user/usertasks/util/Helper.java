package com.blackswam.user.usertasks.util;

import com.blackswam.user.usertasks.exceptions.BadRequestException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Helper {

    /**
     *
     * @param date
     * @return
     * @throws BadRequestException
     */
    public static DateTimeVO getLocalDateTime(String date) throws BadRequestException {
        LocalDateTime dateTime = null;
        DateTimeVO dateTimeVO = new DateTimeVO();
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
             dateTime = LocalDateTime.parse(date, formatter);
             dateTimeVO.setValid(true);
             dateTimeVO.setLocalDateTime(dateTime);
        }catch(Exception e){
            throw new BadRequestException("2001","Invalid Date format Exception. Date should be yyyy-MM-dd HH:mm");
        }

        return dateTimeVO;
     }


    /**
     *
     */

    public static class DateTimeVO{
         private boolean isValid;
         private LocalDateTime localDateTime;

         public boolean isValid() {
             return isValid;
         }

         public void setValid(boolean valid) {
             isValid = valid;
         }

         public LocalDateTime getLocalDateTime() {
             return localDateTime;
         }

         public void setLocalDateTime(LocalDateTime localDateTime) {
             this.localDateTime = localDateTime;
         }



     }


}
