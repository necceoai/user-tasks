package com.blackswam.user.usertasks.factory;

import com.blackswam.user.usertasks.entity.TaskEntity;
import com.blackswam.user.usertasks.entity.UserEntity;
import com.blackswam.user.usertasks.exceptions.BadRequestException;
import com.blackswam.user.usertasks.model.Task;
import com.blackswam.user.usertasks.model.User;
import com.blackswam.user.usertasks.util.Helper;

public class Factory {


    /**
     *
     * @param taskEntity
     * @return
     */
    public static Task createWith(TaskEntity taskEntity){
       Task task = new Task();
       task.setId(taskEntity.getId());
       task.setUserId(taskEntity.getUserId());
       task.setName(taskEntity.getName());
       task.setStatus(taskEntity.getStatus());
       task.setDescription(taskEntity.getDescription());
       task.setCreatedDate(taskEntity.getCreatedDate());
       task.setUpdatedDate(taskEntity.getUpdatedDate());
       task.setDateTime(taskEntity.getDateTime().toString());
       return task;

    }


    /***
     *
     * @param task
     * @return
     * @throws BadRequestException
     */
    public static TaskEntity createWith(Task task) throws BadRequestException {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(task.getId());
        taskEntity.setUserId(task.getUserId());
        taskEntity.setName(task.getName());
        taskEntity.setStatus(task.getStatus());
        taskEntity.setDescription(task.getDescription());
        taskEntity.setCreatedDate(task.getCreatedDate());
        taskEntity.setUpdatedDate(task.getUpdatedDate());
        taskEntity.setDateTime(Helper.getLocalDateTime(task.getDateTime()).getLocalDateTime());
        return taskEntity;
    }


    /**
     *
     * @param user
     * @return
     */

    public static UserEntity createWith(User user){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId());
        userEntity.setUserName(user.getUserName());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setUpdatedDate(user.getUpdatedDate());
        userEntity.setCreatedDate(user.getCreatedDate());

        return userEntity;
    }

    /**
     *
     * @param userEntity
     * @return
     */
    public static User createWith(UserEntity userEntity){
        User user = new User();
        user.setId(userEntity.getId());
        user.setUserName(userEntity.getUserName());
        user.setFirstName(userEntity.getFirstName());
        user.setLastName(userEntity.getLastName());
        user.setUpdatedDate(userEntity.getUpdatedDate());
        user.setCreatedDate(userEntity.getCreatedDate());

        return user;
    }

}
