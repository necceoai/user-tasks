# user-tasks

1) Swgger file is available in the project location.
2) Swagger URL - https://api.stoplight.io/v1/versions/oExSiHJ24kbYJk4Ws/export/stoplight.yaml
3) Service endpoints
	i) User Service -> http://localhost:8080/api/user
	ii)Task Service -> http://localhost:8080/api/user/tasks
4) Quartz scheduler runs for every 2 seconds to avoid waiting time and prints in the console.
5) Very minimal test cases are covered.
6) To run the service use the command mvn spring-boot:run